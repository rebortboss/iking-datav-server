package com.ikingtech.platform.datav.release.service;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.IdUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ikingtech.framework.sdk.base.model.PageResult;
import com.ikingtech.framework.sdk.context.exception.FrameworkException;
import com.ikingtech.framework.sdk.utils.Tools;
import com.ikingtech.platform.datav.release.entity.ReleaseDO;
import com.ikingtech.platform.datav.release.exception.ReleaseExceptionInfo;
import com.ikingtech.platform.datav.release.mapper.ReleaseMapper;
import com.ikingtech.platform.datav.release.pojo.ReleaseDTO;
import com.ikingtech.platform.datav.release.pojo.ReleaseSearchDTO;
import com.ikingtech.platform.datav.release.pojo.ReleaseVO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author fucb
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class ReleaseService extends ServiceImpl<ReleaseMapper, ReleaseDO> {

    public PageResult<ReleaseDO> selectByPage(ReleaseSearchDTO queryParam) {
        return PageResult.build(this.page(new Page<>(queryParam.getPage(), queryParam.getRows()), Wrappers.<ReleaseDO>lambdaQuery()
                .like(Tools.Str.isNotBlank(queryParam.getName()), ReleaseDO::getName, queryParam.getName())
                .like(Tools.Str.isNotBlank(queryParam.getUserName()), ReleaseDO::getUserName, queryParam.getUserName())
                .eq(Tools.Str.isNotBlank(queryParam.getId()), ReleaseDO::getId, queryParam.getId())
                .eq(Tools.Str.isNotBlank(queryParam.getProjectId()), ReleaseDO::getProjectId, queryParam.getProjectId())
                .eq(Tools.Str.isNotBlank(queryParam.getScreenId()), ReleaseDO::getScreenId, queryParam.getScreenId())));
    }

    public ReleaseVO getInfo(String id) {
        ReleaseDO entity = this.baseMapper.selectById(id);
        if (null == entity) {
            throw new FrameworkException(ReleaseExceptionInfo.RELEASE_NOT_FOUND);
        }
        return BeanUtil.copyProperties(entity, ReleaseVO.class);
    }

    @Transactional(rollbackFor = Exception.class)
    public void add(ReleaseDTO param) {
        ReleaseDO release = BeanUtil.copyProperties(param, ReleaseDO.class);
        release.setId(IdUtil.simpleUUID());
        this.baseMapper.insert(release);
    }

    public void edit(ReleaseDTO param) {
        if (!this.exist(param.getId())) {
            throw new FrameworkException(ReleaseExceptionInfo.RELEASE_NOT_FOUND);
        }
        ReleaseDO release = BeanUtil.copyProperties(param, ReleaseDO.class);
        this.baseMapper.updateById(release);
    }

    public void delete(String id) {
        this.baseMapper.deleteById(id);
    }

    public boolean exist(String id) {
        return this.baseMapper.exists(Wrappers.<ReleaseDO>lambdaQuery().eq(ReleaseDO::getId, id));
    }
}
