package com.ikingtech.platform.datav.info.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ikingtech.platform.datav.info.entity.InfoDO;

/**
 * @author fucb
 */
public interface InfoMapper extends BaseMapper<InfoDO> {

}

