package com.ikingtech.platform.datav.info.model;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * @author fucb
 */

@Data
@Schema(name = "InfoDTO对象", description = "基础信息表对象")
public class InfoDTO {

    private String id;

    /**
     * 大屏ID
     */
    @Schema(name = "screenId", description = "大屏ID")
    private String screenId;

    /**
     * 名称
     */
    @Schema(name = "name", description = "名称")
    private String name;

    /**
     * 组ID
     */
    @Schema(name = "groupId", description = "组ID")
    private String groupId;

    /**
     * 模板ID
     */
    @Schema(name = "templateId", description = "模板ID")
    private String templateId;

    /**
     * 分享地址
     */
    @Schema(name = "shareUrl", description = "分享地址")
    private String shareUrl;

    /**
     * 分享密码
     */
    @Schema(name = "sharePassword", description = "分享密码")
    private String sharePassword;

    /**
     * 分享过期时间
     */
    @Schema(name = "shareTime", description = "分享过期时间")
    private Date shareTime;

    /**
     * 是否已发布
     */
    @Schema(name = "shared", description = "是否已发布")
    private Boolean shared;

    /**
     * 宽度
     */
    @Schema(name = "width", description = "宽度")
    private Integer width;

    /**
     * 高度
     */
    @Schema(name = "height", description = "高度")
    private Integer height;

    /**
     * 背景图
     */
    @Schema(name = "bgimage", description = "背景图")
    private String bgimage;

    /**
     * 背景色
     */
    @Schema(name = "bgcolor", description = "背景色")
    private String bgcolor;

    /**
     * 间隔像素
     */
    @Schema(name = "grid", description = "间隔像素")
    private Integer grid;

    /**
     * 缩略图
     */
    @Schema(name = "screenshot", description = "缩略图")
    private String screenshot;

    /**
     * 使用水印
     */
    @Schema(name = "useWatermark", description = "使用水印")
    private Boolean useWatermark;

    /**
     * 滤镜
     */
    @Schema(name = "styleFilterParams", description = "滤镜")
    private Object styleFilterParams;

    /**
     * 子屏
     */
    @Schema(name = "pages", description = "子屏")
    private List<Object> pages;

    /**
     * 弹窗
     */
    @Schema(name = "dialogs", description = "弹窗")
    private List<Object> dialogs;

    /**
     * 请求配置
     */
    @Schema(name = "address", description = "请求配置")
    private List<Object> address;

    /**
     * 变量
     */
    @Schema(name = "variables", description = "变量")
    private Object variables;

    /**
     * 模板图片地址
     */
    @Schema(name = "thumbnail", description = "模板图片地址")
    private String thumbnail;

    /**
     * 缩放方式
     */
    @Schema(name = "zoomMode", description = "缩放方式")
    private Integer zoomMode;

    /**
     * HOST配置
     */
    @Schema(name = "host", description = "HOST配置")
    private List<Object> host;

    /**
     * 全局事件
     */
    @Schema(name = "events", description = "全局事件")
    private List<Object> events;

    /**
     * 默认子屏
     */
    @Schema(name = "defaultPage", description = "默认子屏")
    private String defaultPage;

    /**
     * 嵌入配置
     */
    @Schema(name = "iframe", description = "嵌入配置")
    private Object iframe;

}

