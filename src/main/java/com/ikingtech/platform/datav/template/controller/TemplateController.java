package com.ikingtech.platform.datav.template.controller;

import cn.hutool.core.bean.BeanUtil;
import com.ikingtech.framework.sdk.core.response.R;
import com.ikingtech.framework.sdk.web.annotation.ApiController;
import com.ikingtech.framework.sdk.web.annotation.DeleteRequest;
import com.ikingtech.framework.sdk.web.annotation.GetRequest;
import com.ikingtech.framework.sdk.web.annotation.PostRequest;
import com.ikingtech.platform.datav.info.entity.InfoDO;
import com.ikingtech.platform.datav.info.service.InfoService;
import com.ikingtech.platform.datav.template.entity.TemplateDO;
import com.ikingtech.platform.datav.template.model.TemplateDTO;
import com.ikingtech.platform.datav.template.model.TemplatePublishDTO;
import com.ikingtech.platform.datav.template.model.TemplateSearchDTO;
import com.ikingtech.platform.datav.template.model.TemplateVO;
import com.ikingtech.platform.datav.template.service.TemplateService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author fucb
 */
@RestController
@RequiredArgsConstructor
@ApiController(value = "/template", name = "可视化大屏-模板管理", description = "可视化大屏-模板管理")
public class TemplateController {

    private final TemplateService templateService;

    private final InfoService infoService;

    @PostRequest(value = "/page", summary = "查询-分页查询", description = "查询-分页查询")
    public R<List<TemplateVO>> selectByPage(@RequestBody(required = false) TemplateSearchDTO param) {
        if (param == null) {
            param = new TemplateSearchDTO();
        }
        if (param.getRows() <= 0) {
            param.setRows(15);
        }
        return R.ok(this.templateService.selectByPage(param).convert(this::modelConvert));
    }

    @GetRequest(value = "/get/info", summary = "查询详情", description = "查询详情")
    public R<TemplateVO> getInfo(@RequestParam("id") String id) {
        return R.ok(this.templateService.getInfo(id));
    }

    @PostRequest(value = "/add", summary = "新增模板", description = "新增模板")
    public R<String> add(@RequestBody TemplatePublishDTO param) {
        this.templateService.add(param.getTemplate());
        return R.ok();
    }

    @PostRequest(value = "/turn", summary = "大屏转为模板（发布）", description = "大屏转为模板（发布）")
    public R<String> turn(@RequestBody TemplatePublishDTO param) {
        InfoDO byId = infoService.getById(param.getScreen().getId());
        templateService.turn(param, byId);
        return R.ok();
    }

    @PostRequest(value = "/edit", summary = "编辑模板", description = "编辑模板")
    public R<String> edit(@RequestBody TemplateDTO param) {
        this.templateService.edit(param);
        return R.ok();
    }

    @DeleteRequest(value = "/delete", summary = "删除", description = "删除")
    public R<String> delete(@RequestParam("id") String id) {
        this.templateService.delete(id);
        return R.ok();
    }

    private TemplateVO modelConvert(TemplateDO entity) {
        return BeanUtil.copyProperties(entity, TemplateVO.class);
    }
}

