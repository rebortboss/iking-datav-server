package com.ikingtech.platform.datav.template.exception;

import com.ikingtech.framework.sdk.context.exception.FrameworkExceptionInfo;
import lombok.RequiredArgsConstructor;

/**
 * @author fucb
 */

@RequiredArgsConstructor
public enum TemplateExceptionInfo implements FrameworkExceptionInfo {

    /**
     * 模板不存在
     */
    TEMPLATE_NOT_FOUND("templateNotFound"),
    ;

    private final String message;

    @Override
    public String message() {
        return this.message;
    }

    @Override
    public String moduleName() {
        return "iking-framework-maintenace-datav";
    }
}
