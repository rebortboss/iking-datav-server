package com.ikingtech.platform.datav.component.service;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fasterxml.jackson.core.type.TypeReference;
import com.ikingtech.framework.sdk.base.model.PageResult;
import com.ikingtech.framework.sdk.context.exception.FrameworkException;
import com.ikingtech.framework.sdk.utils.Tools;
import com.ikingtech.platform.datav.component.entity.ComponentDO;
import com.ikingtech.platform.datav.component.exception.ComponentExceptionInfo;
import com.ikingtech.platform.datav.component.mapper.ComponentMapper;
import com.ikingtech.platform.datav.component.model.ComponentDTO;
import com.ikingtech.platform.datav.component.model.ComponentSearchDTO;
import com.ikingtech.platform.datav.component.model.ComponentVO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * 组件表(ComponentDO)服务实现类
 *
 * @author fucb
 * @since 2024-02-22 09:28:43
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class ComponentService extends ServiceImpl<ComponentMapper, ComponentDO> {


    /**
     * 分页查询
     *
     * @param queryParam 筛选条件
     * @return 查询结果
     */
    public PageResult<ComponentDO> selectByPage(ComponentSearchDTO queryParam) {
        return PageResult.build(this.page(new Page<>(queryParam.getPage(), queryParam.getRows()), Wrappers.<ComponentDO>lambdaQuery()
                .like(Tools.Str.isNotBlank(queryParam.getName()), ComponentDO::getName, queryParam.getName())
                .eq(Tools.Str.isNotBlank(queryParam.getId()), ComponentDO::getId, queryParam.getId())
                .eq(Tools.Str.isNotBlank(queryParam.getProjectId()), ComponentDO::getProjectId, queryParam.getProjectId())
                .eq(Tools.Str.isNotBlank(queryParam.getScreenId()), ComponentDO::getScreenId, queryParam.getScreenId())
                .eq(Tools.Str.isNotBlank(queryParam.getParentId()), ComponentDO::getParentId, queryParam.getParentId())
                .eq(Objects.nonNull(queryParam.getHided()), ComponentDO::getHided, queryParam.getHided())
                .eq(Objects.nonNull(queryParam.getHovered()), ComponentDO::getHovered, queryParam.getHovered())
                .eq(Objects.nonNull(queryParam.getIsDialog()), ComponentDO::getIsDialog, queryParam.getIsDialog())
                .eq(Objects.nonNull(queryParam.getIsPublic()), ComponentDO::getIsPublic, queryParam.getIsPublic())
                .eq(Tools.Str.isNotBlank(queryParam.getTemplateId()), ComponentDO::getTemplateId, queryParam.getTemplateId())
                .eq(Tools.Str.isNotBlank(queryParam.getComponentType()), ComponentDO::getComponentType, queryParam.getComponentType())
                .like(Tools.Str.isNotBlank(queryParam.getAlias()), ComponentDO::getAlias, queryParam.getAlias())
                .eq(Objects.nonNull(queryParam.getVersion()), ComponentDO::getVersion, queryParam.getVersion())));
    }

    /**
     * 查询详情
     *
     * @param id 主键
     * @return 数据详情
     */
    public ComponentVO getInfo(String id) {
        ComponentDO entity = this.baseMapper.selectById(id);
        if (null == entity) {
            throw new FrameworkException(ComponentExceptionInfo.COMPONENT_NOT_FOUND);
        }
        return BeanUtil.copyProperties(entity, ComponentVO.class);
    }

    /**
     * 新增
     *
     * @param param 实例对象
     */
    @Transactional(rollbackFor = Exception.class)
    public void add(ComponentDTO param) {
        this.baseMapper.insert(this.extracted(param));
    }

    public ComponentDO extracted(ComponentDTO param) {
        ComponentDO component = BeanUtil.copyProperties(param, ComponentDO.class);
        component.setAnimate(Tools.Json.toJsonStr(param.getAnimate()));
        component.setApis(Tools.Json.toJsonStr(param.getApis()));
        component.setApiData(Tools.Json.toJsonStr(param.getApiData()));
        component.setAttr(Tools.Json.toJsonStr(param.getAttr()));
        component.setBgImg(Tools.Json.toJsonStr(param.getBgImg()));
        component.setConfig(Tools.Json.toJsonStr(param.getConfig()));
        component.setDialog(Tools.Json.toJsonStr(param.getDialog()));
        component.setDisActions(Tools.Json.toJsonStr(param.getDisActions()));
        component.setEvents(Tools.Json.toJsonStr(param.getEvents()));
        component.setScaling(Tools.Json.toJsonStr(param.getScaling()));
        component.setChildren(Tools.Json.toJsonStr(param.getChildren()));
        return component;
    }

    /**
     * 修改
     *
     * @param param 实例对象
     */
    public void edit(ComponentDTO param) {
        if (!this.exist(param.getId())) {
            throw new FrameworkException(ComponentExceptionInfo.COMPONENT_NOT_FOUND);
        }

        ComponentDO component = BeanUtil.copyProperties(param, ComponentDO.class);
        this.baseMapper.updateById(component);
    }

    /**
     * 删除
     *
     * @param id 数据id
     */
    public void delete(String id) {
        this.baseMapper.deleteById(id);
    }

    public boolean exist(String id) {
        return this.baseMapper.exists(Wrappers.<ComponentDO>lambdaQuery().eq(ComponentDO::getId, id));
    }

    public List<ComponentVO> convertVO(List<ComponentDO> list) {
        return list.stream().map(e -> {
            ComponentVO componentVO = BeanUtil.copyProperties(e, ComponentVO.class);
            componentVO.setAnimate(Tools.Json.toBean(e.getAnimate(), new TypeReference<>() {
            }));
            componentVO.setApis(Tools.Json.toBean(e.getApis(), new TypeReference<>() {
            }));
            componentVO.setApiData(Tools.Json.toBean(e.getApiData(), new TypeReference<>() {
            }));
            componentVO.setAttr(Tools.Json.toBean(e.getAttr(), new TypeReference<>() {
            }));
            componentVO.setBgImg(Tools.Json.toBean(e.getBgImg(), new TypeReference<>() {
            }));
            componentVO.setConfig(Tools.Json.toBean(e.getConfig(), new TypeReference<>() {
            }));
            componentVO.setDialog(Tools.Json.toBean(e.getDialog(), new TypeReference<>() {
            }));
            componentVO.setDisActions(Tools.Json.toBean(e.getDisActions(), new TypeReference<>() {
            }));
            componentVO.setEvents(Tools.Json.toBean(e.getEvents(), new TypeReference<>() {
            }));
            componentVO.setScaling(Tools.Json.toBean(e.getScaling(), new TypeReference<>() {
            }));
            componentVO.setChildren(Tools.Json.toBean(e.getChildren(), new TypeReference<>() {
            }));
            return componentVO;
        }).collect(Collectors.toList());
    }

    public List<ComponentDO> getListByScreenId(String screenId) {
        LambdaQueryWrapper<ComponentDO> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(ComponentDO::getScreenId, screenId);
        return this.list(queryWrapper);
    }

    public List<ComponentDO> getListByScreenIds(List<String> screenId) {
        LambdaQueryWrapper<ComponentDO> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.in(ComponentDO::getScreenId, screenId);
        return this.list(queryWrapper);
    }

    public void removeByScreenId(String id) {
        LambdaQueryWrapper<ComponentDO> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(ComponentDO::getScreenId, id);
        remove(queryWrapper);
    }
}
