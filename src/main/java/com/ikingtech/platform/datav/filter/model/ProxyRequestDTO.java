package com.ikingtech.platform.datav.filter.model;

import cn.hutool.http.Method;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotBlank;
import lombok.Data;

/**
 * created on 2024-03-20 15:37
 *
 * @author wub
 */

@Data
public class ProxyRequestDTO {

    @NotBlank(message = "代理接口必必传")
    @Schema(name = "proxyApi", description = "代理接口")
    private String proxyApi;

    @Schema(name = "proxyMethod", description = "代理接口方法")
    private Method proxyMethod;

}
