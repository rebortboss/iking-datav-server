package com.ikingtech.platform.datav.filter.controller;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.ikingtech.framework.sdk.core.response.R;
import com.ikingtech.framework.sdk.utils.Tools;
import com.ikingtech.framework.sdk.web.annotation.ApiController;
import com.ikingtech.framework.sdk.web.annotation.GetRequest;
import com.ikingtech.framework.sdk.web.annotation.PostRequest;
import com.ikingtech.platform.datav.filter.entity.FilterDO;
import com.ikingtech.platform.datav.filter.model.FilterDTO;
import com.ikingtech.platform.datav.filter.model.FilterSearchDTO;
import com.ikingtech.platform.datav.filter.model.FilterVO;
import com.ikingtech.platform.datav.filter.service.FilterService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author fucb
 */
@RestController
@RequiredArgsConstructor
@ApiController(value = "/filter", name = "可视化大屏-过滤器", description = "可视化大屏-过滤器")
public class FilterController {

    private final FilterService filterService;

    @PostRequest(value = "/select/page", summary = "查询-分页查询", description = "查询-分页查询(COMMON-普通过滤器,TEMPLATE-模板过滤器)")
    public R<List<FilterVO>> selectByPage(@RequestBody FilterSearchDTO param) {
        return R.ok(this.filterService.selectByPage(param).convert(this::modelConvert));
    }

    @GetRequest(value = "/get/info", summary = "查询详情", description = "查询详情")
    public R<FilterVO> getInfo(@RequestParam("id") Integer id) {
        return R.ok(this.filterService.getInfo(id));
    }

    @PostRequest(value = "/add", summary = "新增", description = "新增(COMMON-普通过滤器,TEMPLATE-模板过滤器)")
    public R<String> add(@RequestBody FilterDTO param) {
        this.filterService.add(param);
        return R.ok();
    }

    @PostRequest(value = "/update", summary = "编辑", description = "编辑")
    public R<String> edit(@RequestBody FilterDTO param) {
        this.filterService.edit(param);
        return R.ok();
    }

    @GetRequest(value = "/delete", summary = "删除", description = "删除")
    public R<String> delete(@RequestParam String id) {
        this.filterService.delete(id);
        return R.ok();
    }

    @PostRequest(value = "/lists", summary = "查询-查询大屏的所有过滤器", description = "查询-分页查询(COMMON-普通过滤器,TEMPLATE-模板过滤器)")
    public R<List<FilterVO>> filterLists(@RequestBody FilterSearchDTO param) {
        LambdaQueryWrapper<FilterDO> queryWrapper = new LambdaQueryWrapper<>();
        if (Tools.Str.isNotBlank(param.getProjectId())) {
            queryWrapper.and(q -> q.eq(FilterDO::getProjectId, param.getProjectId()).or().isNull(FilterDO::getProjectId));
        }
        queryWrapper.and(q -> q.eq(FilterDO::getScreenId, param.getScreenId()).or().isNull(FilterDO::getScreenId));
        List<FilterDO> list = this.filterService.list(queryWrapper);
        return R.ok(BeanUtil.copyToList(list, FilterVO.class));
    }

    private FilterVO modelConvert(FilterDO entity) {
        return BeanUtil.copyProperties(entity, FilterVO.class);
    }
}

