package com.ikingtech.platform.datav.interceptor;

import io.undertow.servlet.spec.HttpServletRequestImpl;
import jakarta.servlet.*;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;

/**
 * created on 2024-03-05 11:15
 *
 * @author wub
 */

@Component
public class CustomFilter extends OncePerRequestFilter {

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        ((HttpServletRequestImpl) request).getExchange().addQueryParam("tenant", "sys");
        filterChain.doFilter(request, response);
    }
}
