package com.ikingtech.platform.datav.interceptor;

import com.ikingtech.framework.sdk.context.constant.CommonConstants;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * created on 2024-03-05 11:16
 *
 * @author wub
 */

@Configuration
public class MyFilterConfig implements WebMvcConfigurer {

    @Bean
    public FilterRegistrationBean<CustomFilter> myCustomFilterRegistration() {
        FilterRegistrationBean<CustomFilter> registrationBean = new FilterRegistrationBean<>();
        registrationBean.setFilter(new CustomFilter());
        // 设置过滤器适用的URL模式
        registrationBean.addUrlPatterns("/*");
        // 设置过滤器的名字
        registrationBean.setName("myCustomFilter");
        // 设置过滤器的执行顺序，数字越小越先执行
        registrationBean.setOrder(CommonConstants.SECURITY_FILTER_ORDER - 1);
        return registrationBean;
    }
}
