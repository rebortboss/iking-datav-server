package com.ikingtech.platform.datav.systemconfig.service;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.ikingtech.framework.sdk.base.model.PageResult;
import com.ikingtech.framework.sdk.utils.Tools;
import com.ikingtech.platform.datav.systemconfig.entity.SystemConfig;
import com.ikingtech.platform.datav.systemconfig.mapper.SystemConfigMapper;
import com.ikingtech.platform.datav.systemconfig.pojo.SystemConfigDTO;
import com.ikingtech.platform.datav.systemconfig.pojo.SystemConfigSearchDTO;
import com.ikingtech.platform.datav.systemconfig.pojo.SystemConfigVO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * created on 2024-03-22 14:41
 *
 * @author wub
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class SystemConfigService extends ServiceImpl<SystemConfigMapper, SystemConfig> {

    public void add(SystemConfigDTO param) {
        this.save(BeanUtil.copyProperties(param, SystemConfig.class));
    }

    public void edit(SystemConfigDTO param) {
        this.updateById(BeanUtil.copyProperties(param, SystemConfig.class));
    }

    public void delete(String id) {
        this.removeById(id);
    }

    public SystemConfigVO getInfo(String id) {
        SystemConfig systemConfig = this.baseMapper.selectById(id);
        return BeanUtil.copyProperties(systemConfig, SystemConfigVO.class);
    }

    /**
     * 根据类型获取系统配置信息列表。
     *
     * @param type 系统配置的类型，用于查询条件。
     * @return 返回匹配给定类型的系统配置信息列表。
     */
    public List<SystemConfig> getInfoByType(String type) {
        return this.baseMapper.selectList(Wrappers.<SystemConfig>lambdaQuery().eq(SystemConfig::getType, type));
    }

    public PageResult<SystemConfigVO> selectByPage(SystemConfigSearchDTO param) {
        Page<SystemConfig> page = this.baseMapper.selectPage(new Page<>(param.getPage(), param.getRows()), Wrappers.<SystemConfig>lambdaQuery()
                .eq(Tools.Str.isNotBlank(param.getType()), SystemConfig::getType, param.getType())
                .like(Tools.Str.isNotBlank(param.getProperties()), SystemConfig::getProperties, param.getProperties()));
        return PageResult.build(page.getPages(), page.getTotal(), BeanUtil.copyToList(page.getRecords(), SystemConfigVO.class));
    }
}
