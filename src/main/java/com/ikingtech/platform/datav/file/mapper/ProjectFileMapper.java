package com.ikingtech.platform.datav.file.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.ikingtech.platform.datav.file.entity.ProjectFileDO;

/**
 * @author fucb
 */
public interface ProjectFileMapper extends BaseMapper<ProjectFileDO> {

}

