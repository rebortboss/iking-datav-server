package com.ikingtech.platform.datav.config;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class FileConstant {


    /**
     * 文件基本前缀路径
     */
    public static String FILE_BASE_URL = "/datav/file";
    /**
     * 文件上传路径
     */
    public static String FILE_UPLOAD_PATH = FILE_BASE_URL + "/upload";
    /**
     * 资源包上传路径
     */
    public static String FILE_RESOURCE_PATH = FILE_BASE_URL + "/resource";

}
